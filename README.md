A simple mod that gives you the ability to bind a toggle for skin layers. This is useful for those who want to use the skin layers but don't want to have to go into the menu every time they want to change it.

Feel free to check out https://alinea.gg/.
