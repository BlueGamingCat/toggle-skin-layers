package us.mikebartosh.minecraft.toggleskinlayers;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.render.entity.PlayerModelPart;
import net.minecraft.client.util.InputUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class toggleSkinLayers implements ModInitializer {
    public final KeyBinding toggleHead;
    public final KeyBinding toggleCape;
    public final KeyBinding toggleJacket;
    public final KeyBinding toggleLeftLeg;
    public final KeyBinding toggleLeftArm;
    public final KeyBinding toggleRightArm;
    public final KeyBinding toggleRightLeg;
    public final Map<KeyBinding, PlayerModelPart> toggleKeybinds;

    public toggleSkinLayers() {
        toggleHead = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Head", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));
        toggleCape = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Cape", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));
        toggleJacket = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Jacket", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));
        toggleLeftLeg = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Left Leg", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));
        toggleLeftArm = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Left Arm", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));
        toggleRightArm = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Right Arm", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));
        toggleRightLeg = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle Right Leg", InputUtil.UNKNOWN_KEY.getCode(), "Toggle Skin Layers"));

        toggleKeybinds = new HashMap<KeyBinding, PlayerModelPart>();
        toggleKeybinds.put(toggleHead, PlayerModelPart.HAT);
        toggleKeybinds.put(toggleCape, PlayerModelPart.CAPE);
        toggleKeybinds.put(toggleJacket, PlayerModelPart.JACKET);
        toggleKeybinds.put(toggleLeftLeg, PlayerModelPart.LEFT_PANTS_LEG);
        toggleKeybinds.put(toggleLeftArm, PlayerModelPart.LEFT_SLEEVE);
        toggleKeybinds.put(toggleRightArm, PlayerModelPart.RIGHT_SLEEVE);
        toggleKeybinds.put(toggleRightLeg, PlayerModelPart.RIGHT_PANTS_LEG);
    }

    @Override
    public void onInitialize() {
        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            Map<InputUtil.Key, List<KeyBinding>> multiToggleKeybinds = new HashMap<>();
            for (KeyBinding kb : toggleKeybinds.keySet()) {
                multiToggleKeybinds.computeIfAbsent(InputUtil.fromTranslationKey(kb.getBoundKeyTranslationKey()), l -> new ArrayList<>()).add(kb);
            }

            for (KeyBinding kb : toggleKeybinds.keySet()) {
                if (kb.wasPressed()) {
                    for (KeyBinding key : multiToggleKeybinds.get(InputUtil.fromTranslationKey(kb.getBoundKeyTranslationKey()))) {
                        PlayerModelPart part = toggleKeybinds.get(key);
                        boolean isPartEnabled = client.options.isPlayerModelPartEnabled(part);
                        client.options.togglePlayerModelPart(part, !isPartEnabled);
                    }
                }
            }
        });
    }
}
